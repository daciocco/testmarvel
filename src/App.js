
import './App.css';
import axios from 'axios';
import { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card'
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';

import { Form } from 'react-bootstrap';
import { InputGroup } from 'react-bootstrap';
import { FormControl } from 'react-bootstrap';

import { 
  ImSortAmountAsc, 
  ImSortAmountDesc
} from "react-icons/im";

//https://gateway.marvel.com:443/v1/public/characters?apikey=78dd3bc18c11051da80222c6609a580e
//key privada: a91c656d8c931c98209b1ae988861048754fa552
//key publica: 78dd3bc18c11051da80222c6609a580e
// ts: 1
// para sacar el md5: ts+privada+publica:  1a91c656d8c931c98209b1ae988861048754fa55278dd3bc18c11051da80222c6609a580e
//mg5 hash: 4a93c77db951e9c90d0016f4883fdf85

function App() {

  const [ personajes, setPersonajes ] = useState([]);

  const [ search, setSearch ] = useState('');
  const [ filteredPersonajes, setFilteredPersonajes ] = useState([]);

  const [ sortType, setSortType ] = useState('asc');

  //const [ order, setOrder ] = useState('asc');

  //Llamas a lista de datos de MARVEL
  useEffect(()=>{
    axios.get('https://gateway.marvel.com:443/v1/public/characters?ts=1&apikey=78dd3bc18c11051da80222c6609a580e&hash=4a93c77db951e9c90d0016f4883fdf85').then(res=>{
      setPersonajes(res.data.data.results);
    }).catch(error=>console.log(error))
  }, [])


  //Contrucción de Filtro para personajes por campo Nombre
  useEffect(()=>{
    setFilteredPersonajes(
      personajes
      .filter( per => {
        return per.name.toLowerCase().includes( search.toLowerCase() )
      })
      .sort( 
        ( sortType === "asc" ) ?
          (a, b) => a.name > b.name ? 1 : -1 
        :
          (b, a) => a.name > b.name ? 1 : -1 
      )
    )
  }, [search, sortType, personajes])


  return (
    <div className="App">

      <Image
       width={171}
       alt="marvel-logo"
       src="http://localhost:3000/marvel-logo-4.png"
      />

      <Row className="justify-content-md-center filters">

        <Col md lg="3" >
          <Form.Group md="4">
            <InputGroup className="mb-3">
              <InputGroup.Text id="basic-addon1">Filtrar por Título</InputGroup.Text>
              <FormControl
                aria-label="Filtrar"
                onKeyUp={ e => setSearch( e.target.value ) }
              />

            </InputGroup>            
          </Form.Group>
        </Col>
        
        <Col md lg="3">
            <InputGroup className="md-3" >   
              <InputGroup.Text id="basic-addon1" ><ImSortAmountAsc/> </InputGroup.Text>
              <InputGroup.Checkbox name="group1" type='radio' aria-label="ASC" defaultChecked='checked' value="asc" onChange={e => setSortType(e.currentTarget.value)}/>
              <InputGroup.Text id="basic-addon1"><ImSortAmountDesc/></InputGroup.Text>
              <InputGroup.Checkbox name="group1" type='radio' aria-label="DESC" value="desc" onChange={e => setSortType(e.currentTarget.value)}/>
            </InputGroup>
        </Col>        
        
      </Row>
      
      
      <Row className='card-row'>

        { 
        
        /* <Card.Title>{per.name}</Card.Title> */
        
        filteredPersonajes.map( per =>(

            <Card
              style={{ width: '18rem' }} 
              key={per.id} 
              onClick={(e) => {
                e.preventDefault();
                window.open(
                  `${per.urls[2].url}`,
                  '_blank'
                );
              }}>

              
              <Card.Header>{per.name}</Card.Header>
              <Card.Img variant="top" src={`${per.thumbnail.path}.${per.thumbnail.extension}`} />
              <Card.Body>
              
                
                <Card.Text>                  
                  {per.description}
                </Card.Text>
                
              </Card.Body>
            </Card>

        ))
        }

      </Row>

      
    </div>
  );
}

export default App;
